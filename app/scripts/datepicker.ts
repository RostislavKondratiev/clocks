declare var $: any;
declare var Handlebars: any;
declare var moment;
$( function() {
    let entrySource  = $("#entry").html();
    let entryTemplate = Handlebars.compile(entrySource);
    let listSource = $('#list').html();
    let listTemplate = Handlebars.compile(listSource);
    let entryContext = {raw: 'Not selected', relative: 'Not selected', calendar: 'Not selected'};
    $('.selected-date').append(entryTemplate(entryContext));
    let datesContext: any = {dates: []};
    $( "#datepicker" ).datepicker({
        onSelect: (dateText: any, inst: any, extensionRange: any) => {
            let targetDate = moment(dateText);
            let newEntryContext = {
                raw: targetDate.format('MMMM Do YYYY, h:mm:ss a'),
                relative: targetDate.endOf('day').fromNow(),
                calendar: targetDate.calendar()
            }
            datesContext.dates.push(targetDate.format('MMM Do YY'));
            $('.entry').remove();
            $('.selected-date').append(entryTemplate(newEntryContext));
            $('.list').remove();
            $('.dates-history').append(listTemplate(datesContext));
        }
    });
})