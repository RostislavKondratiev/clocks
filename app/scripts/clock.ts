declare var d3: any;
declare var moment: any;
declare var Handlebars: any;
declare var timezone: any;

class Clock {
    private svg: any;
    private wrap: any;
    private cx: number;
    private cy: number;
    private margin: number = 4;
    private r: number;
    constructor(private width: number, private height: number, public container: string, public timezone?: string, private format?: string) {
        this.cx = this.width / 2;
        this.cy = this.height / 2;
        this.r = this.width / 2 - this.margin;
    }

    public get location(){
        return this.timezone || 'Current';
    }

    public createClock(){
        this.clockMarkUp();
        this.makeClockFace();
        this.createHands();
        this.displayDigitalTime();
        this.displayLocation();
        setInterval(this.updateHands.bind(this), 1000)
    }
    private clockMarkUp() {
        let clockTemplate = Handlebars.compile(`
            <div class = "a ${this.container}"></div>
        `)
        $('.clocks-wrap').append(clockTemplate(true));
        this.svg = d3.select(`.${this.container}`)
        .append("svg")
        .attr("class", "clock")
        .attr("width", this.width)
        .attr("height", this.height)
    }
    public getTime() {
        return this.timezone ? moment().tz(this.timezone) : moment();
    }
    private getTimeForText(now: any, format?: any) {
        return now.format(this.format|| format || 'LTE')
    }
    private getTimeForClocks(now: any) {
        let hr = now.hour()
        let min = now.minute()
        let sec = now.second()
        return [
            [ "hour",   hr + (min / 60) + (sec / 3600) ],
            [ "minute", min + (sec / 60) ],
            [ "second", sec ]
        ]
    }
    private handLength(d: any) {
        if( d[0] == 'hour'){
            return Math.round(0.45 * this.r);}
        else {
            return Math.round(0.90 * this.r);}
    }
    private handBackLength(d: any) {
        if(d[0] == 'second')
            return Math.round(0.25 * this.r);
        else
            return Math.round(0.10 * this.r);
    }
    private rotationTransform(d: any) {
        let angle;
        if (d[0] == 'hour')
            angle = (d[1] % 12) * 30
        else
            angle = d[1] * 6
        return `rotate(${angle},${this.cx},${this.cy})`
    }
    private makeClockFace() {
        let hourTickLength = Math.round(this.r * 0.2);
        let minuteTickLength = Math.round(this.r * 0.075);
        for (let i = 0; i < 60; ++i) {
            let tickLength, tickClass;
            if((i % 5) == 0) {
                tickLength = hourTickLength;
                tickClass = 'hourtick'
            }
            else {
                tickLength = minuteTickLength;
                tickClass = 'minutetick';
            }
            this.svg.append("line")
                .attr("class", tickClass + " face")
                .attr("x1", this.cx)
                .attr("y1", this.margin)
                .attr("x2", this.cx)
                .attr("y2", this.margin + tickLength)
                .attr("transform", `rotate(${i * 6},${this.cx},${this.cy})`)
            }
    }
    private createHands() {
        this.svg.selectAll("line.hand")
            .data(this.getTimeForClocks(this.getTime()))
            .enter()
            .append("line")
            .attr("class", (d: any) => {return d[0] + " hand"})
            .attr("x1", this.cx)
            .attr("y1",  (d: any) => {return this.cy + this.handBackLength(d)})
            .attr("x2", this.cx)
            .attr("y2",  (d: any) => { return this.r - this.handLength(d)})
            .attr("transform", this.rotationTransform.bind(this));
    }
    private displayDigitalTime() {
        this.svg.append('text')
            .text(this.getTimeForText(this.getTime()))
            .attr('text-anchor', 'middle')
            .attr('font-size', 25)
            .attr('x', this.width / 2)
            .attr('y', this.height * 0.42)
            .attr('font-family', 'sans-serif')
            .attr('fill', 'green')
            .attr('stroke', 'black')
    }
    private updateHands() {
        this.svg.selectAll("line.hand")
            .data(this.getTimeForClocks(this.getTime()))
            .transition().ease("bounce")
            .attr("transform", this.rotationTransform.bind(this));
        this.svg.selectAll('text')
            .text(this.getTimeForText(this.getTime()));
        }
    private displayLocation() {
        $(`.${this.container}`).append(`<p>${this.location}</p>`).addClass('location');
    }
}