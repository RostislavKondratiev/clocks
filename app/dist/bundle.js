var Clock = (function () {
    function Clock(width, height, container, timezone, format) {
        this.width = width;
        this.height = height;
        this.container = container;
        this.timezone = timezone;
        this.format = format;
        this.margin = 4;
        this.cx = this.width / 2;
        this.cy = this.height / 2;
        this.r = this.width / 2 - this.margin;
    }
    Object.defineProperty(Clock.prototype, "location", {
        get: function () {
            return this.timezone || 'Current';
        },
        enumerable: true,
        configurable: true
    });
    Clock.prototype.createClock = function () {
        this.clockMarkUp();
        this.makeClockFace();
        this.createHands();
        this.displayDigitalTime();
        this.displayLocation();
        setInterval(this.updateHands.bind(this), 1000);
    };
    Clock.prototype.clockMarkUp = function () {
        var clockTemplate = Handlebars.compile("\n            <div class = \"a " + this.container + "\"></div>\n        ");
        $('.clocks-wrap').append(clockTemplate(true));
        this.svg = d3.select("." + this.container)
            .append("svg")
            .attr("class", "clock")
            .attr("width", this.width)
            .attr("height", this.height);
    };
    Clock.prototype.getTime = function () {
        return this.timezone ? moment().tz(this.timezone) : moment();
    };
    Clock.prototype.getTimeForText = function (now, format) {
        return now.format(this.format || format || 'LTE');
    };
    Clock.prototype.getTimeForClocks = function (now) {
        var hr = now.hour();
        var min = now.minute();
        var sec = now.second();
        return [
            ["hour", hr + (min / 60) + (sec / 3600)],
            ["minute", min + (sec / 60)],
            ["second", sec]
        ];
    };
    Clock.prototype.handLength = function (d) {
        if (d[0] == 'hour') {
            return Math.round(0.45 * this.r);
        }
        else {
            return Math.round(0.90 * this.r);
        }
    };
    Clock.prototype.handBackLength = function (d) {
        if (d[0] == 'second')
            return Math.round(0.25 * this.r);
        else
            return Math.round(0.10 * this.r);
    };
    Clock.prototype.rotationTransform = function (d) {
        var angle;
        if (d[0] == 'hour')
            angle = (d[1] % 12) * 30;
        else
            angle = d[1] * 6;
        return "rotate(" + angle + "," + this.cx + "," + this.cy + ")";
    };
    Clock.prototype.makeClockFace = function () {
        var hourTickLength = Math.round(this.r * 0.2);
        var minuteTickLength = Math.round(this.r * 0.075);
        for (var i = 0; i < 60; ++i) {
            var tickLength = void 0, tickClass = void 0;
            if ((i % 5) == 0) {
                tickLength = hourTickLength;
                tickClass = 'hourtick';
            }
            else {
                tickLength = minuteTickLength;
                tickClass = 'minutetick';
            }
            this.svg.append("line")
                .attr("class", tickClass + " face")
                .attr("x1", this.cx)
                .attr("y1", this.margin)
                .attr("x2", this.cx)
                .attr("y2", this.margin + tickLength)
                .attr("transform", "rotate(" + i * 6 + "," + this.cx + "," + this.cy + ")");
        }
    };
    Clock.prototype.createHands = function () {
        var _this = this;
        this.svg.selectAll("line.hand")
            .data(this.getTimeForClocks(this.getTime()))
            .enter()
            .append("line")
            .attr("class", function (d) { return d[0] + " hand"; })
            .attr("x1", this.cx)
            .attr("y1", function (d) { return _this.cy + _this.handBackLength(d); })
            .attr("x2", this.cx)
            .attr("y2", function (d) { return _this.r - _this.handLength(d); })
            .attr("transform", this.rotationTransform.bind(this));
    };
    Clock.prototype.displayDigitalTime = function () {
        this.svg.append('text')
            .text(this.getTimeForText(this.getTime()))
            .attr('text-anchor', 'middle')
            .attr('font-size', 25)
            .attr('x', this.width / 2)
            .attr('y', this.height * 0.42)
            .attr('font-family', 'sans-serif')
            .attr('fill', 'green')
            .attr('stroke', 'black');
    };
    Clock.prototype.updateHands = function () {
        this.svg.selectAll("line.hand")
            .data(this.getTimeForClocks(this.getTime()))
            .transition().ease("bounce")
            .attr("transform", this.rotationTransform.bind(this));
        this.svg.selectAll('text')
            .text(this.getTimeForText(this.getTime()));
    };
    Clock.prototype.displayLocation = function () {
        $("." + this.container).append("<p>" + this.location + "</p>").addClass('location');
    };
    return Clock;
}());

var current = new Clock(320, 320, 'container1').createClock();
var chicago = new Clock(320, 320, 'container2', "America/Chicago").createClock();
var japan = new Clock(320, 320, 'container3', 'Japan').createClock();

$(function () {
    var entrySource = $("#entry").html();
    var entryTemplate = Handlebars.compile(entrySource);
    var listSource = $('#list').html();
    var listTemplate = Handlebars.compile(listSource);
    var entryContext = { raw: 'Not selected', relative: 'Not selected', calendar: 'Not selected' };
    $('.selected-date').append(entryTemplate(entryContext));
    var datesContext = { dates: [] };
    $("#datepicker").datepicker({
        onSelect: function (dateText, inst, extensionRange) {
            var targetDate = moment(dateText);
            var newEntryContext = {
                raw: targetDate.format('MMMM Do YYYY, h:mm:ss a'),
                relative: targetDate.endOf('day').fromNow(),
                calendar: targetDate.calendar()
            };
            datesContext.dates.push(targetDate.format('MMM Do YY'));
            $('.entry').remove();
            $('.selected-date').append(entryTemplate(newEntryContext));
            $('.list').remove();
            $('.dates-history').append(listTemplate(datesContext));
        }
    });
});
