const gulp = require('gulp');
const ts = require('gulp-typescript');
const browserSync = require('browser-sync');
const concat = require('gulp-concat');
const tsProject = ts.createProject('tsconfig.json');

gulp.task('test', function() {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(concat('bundle.js'))
        .pipe(gulp.dest('app/dist'));
})

gulp.task('server', function(){
    browserSync({
        notify: false,
        server:{
            baseDir: 'app',
        }
    });
});

gulp.task('watch',['server','test'], function(){
    gulp.watch('app/*.html').on('change', browserSync.reload);
    gulp.watch('app/scripts/*.ts', ['test']).on('change', browserSync.reload);
})